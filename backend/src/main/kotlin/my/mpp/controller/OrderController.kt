package my.mpp

import my.mpp.data.ListRequest
import my.mpp.data.ListResponse
import my.mpp.data.Order
import my.mpp.repository.OrdersRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/order")
class OrderController {

    @Autowired
    lateinit var ordersRepository: OrdersRepository

    @PostMapping("/list")
    fun getList(@RequestBody request: ListRequest): ResponseEntity<ListResponse<Order>> {
        if (request.check()) {
            ResponseEntity("Bad request", HttpStatus.BAD_REQUEST)
        }
        val total = ordersRepository.getTotal(request)
        val list = ordersRepository.getList(request)
        return ResponseEntity(ListResponse(total, list), HttpStatus.OK)
    }

    @PostMapping("/save_new_order")
    fun saveNewOrder(@RequestBody order: Order) {
        if (order.check()) {
            ordersRepository.saveNewList(order)
        }
    }

    @PostMapping("/delete_order")
    fun deleteOrder(@RequestBody order: Order) {
        if (order.check()) {
            ordersRepository.deleteOrder(order)
        }
    }

}